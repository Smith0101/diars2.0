﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.DB.Maps
{
    public class AdjuntoMaps : IEntityTypeConfiguration<Adjunto>
    {
        public void Configure(EntityTypeBuilder<Adjunto> builder)
        {
            builder.ToTable("Adjunto");
            builder.HasKey(o => o.Id);
        }
    }
}
