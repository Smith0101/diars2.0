﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;

namespace MiProyecto.DB.Maps
{
    public class InscribirseMap : IEntityTypeConfiguration<Inscribirse>
    {
        public void Configure(EntityTypeBuilder<Inscribirse> builder)
        {

            builder.ToTable("Inscribirse");
            builder.HasKey(o => o.Id);
        }
    }
}
        
 